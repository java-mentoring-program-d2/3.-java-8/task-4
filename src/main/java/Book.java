import java.util.ArrayList;
import java.util.List;

public class Book {
    private final String title;
    private final int numOfPages;
    private final List<Author> authorList;

    public Book(String title, int numOfPages) {
        this.title = title;
        this.numOfPages = numOfPages;
        this.authorList = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public int getNumOfPages() {
        return numOfPages;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", numOfPages=" + numOfPages +
                '}';
    }
}

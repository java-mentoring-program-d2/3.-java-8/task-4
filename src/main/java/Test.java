import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {
    private static Author[] authors;
    private static Book[] books;

    public static void main(String[] args) {
        preparation();

        // I. check if some/all book(s) have more than 200 pages;
        System.out.println(createStream(books).anyMatch(b -> b.getNumOfPages() > 200));
        System.out.println(createStream(books).allMatch(b -> b.getNumOfPages() > 200));
        printDelimiter();


        // II. find the books with max and min number of pages
        Optional<Book> minBook = createStream(books)
                .min(Comparator.comparing(Book::getNumOfPages));
        Optional<Book> maxBook = createStream(books)
                .max(Comparator.comparing(Book::getNumOfPages));

        Consumer<Optional<Book>> printIfExist = opt -> opt.ifPresent(System.out::println);
        printIfExist.accept(minBook);
        printIfExist.accept(maxBook);
        printDelimiter();


        // III. filter books with only single author;
        createStream(books).parallel()
                .filter(book -> book.getAuthorList().size() == 1)
                .forEach(System.out::println);
        printDelimiter();


        // IV. sort the books by number of pages/title;
        createStream(books)
                .sorted(Comparator.comparing(Book::getNumOfPages).thenComparing(Book::getTitle))
                .forEach(System.out::println);
        printDelimiter();


        // V. get list of all titles;
        createStream(books)
                .map(Book::getTitle)
                .collect(Collectors.toList())
        .forEach(System.out::println);
        printDelimiter();


        // VI. print them using forEach method;
        createStream(books)
                .forEach(System.out::println);
        printDelimiter();


        // VII. get distinct list of all authors;
        createStream(books)
                .flatMap(book -> book.getAuthorList().stream())
                .distinct()
                .forEach(System.out::println);
        printDelimiter();


        // VIII. Optional. Use the Optional type for determining the title of the biggest book of some author.
        createStream(authors[3].getBooks())
                .max(Comparator.comparing(Book::getNumOfPages))
                .ifPresent(book -> System.out.println(book.getTitle()));
    }

    private static <T> Stream<T> createStream(T[] array) {
        return Arrays.stream(array);
    }

    private static <T> Stream<T> createStream(Collection<T> collection) {
        return collection.stream();
    }

    private static void printDelimiter() {
        System.out.println("\n ================ \n");
    }

    private static void preparation() {
        authors = new Author[]{
                new Author("Igor Filatov", (short) 37),
                new Author("John Sidorov", (short) 31),
                new Author("Ilya Sergeev", (short) 26),
                new Author("Pavel Kogtev", (short) 33),
                new Author("Stepan Ulipov", (short) 32)
        };

        books = new Book[]{
                new Book("First Book", 150),
                new Book("Second Book", 200),
                new Book("Third Book", 121),
                new Book("Fourth Book", 212),
                new Book("Fifth Book", 350)
        };

        for (int i = 0; i < books.length; i++) {
            Book book = books[i];
            List<Author> list = book.getAuthorList();

            int n = i % 2 == 0 ? 3 : 2;
            for (int j = 0; j < n; j++) {
                Author author = authors[(i + j + ((n % 2) + 1)) % authors.length];
                list.add(author);
                author.getBooks().add(book);
            }
        }

        Author newSingleAuthor = new Author("Single Jack", (short) 44);
        Book newSingleBook = new Book("Single Book", 400);
        newSingleAuthor.getBooks().add(newSingleBook);
        newSingleBook.getAuthorList().add(newSingleAuthor);

        Author[] newAuthors = new Author[6];
        System.arraycopy(authors, 0, newAuthors, 0, authors.length);
        newAuthors[5] = newSingleAuthor;
        authors = newAuthors;

        Book[] newBooks = new Book[6];
        System.arraycopy(books, 0, newBooks, 0, books.length);
        newBooks[5] = newSingleBook;
        books = newBooks;
    }
}
